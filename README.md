# Vue 3 + Vite

这个模板应该可以帮助您开始在 Vite 中使用 Vue 3 进行开发。该模板使用 Vue 3 <script setup>SFC，请查看脚本设置文档以了解更多信息。

## 推荐的 IDE 设置

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (并禁用 Vetur) + [TypeScript Vue 插件 (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).


更多Vue页面布局详细玩法欢迎访问[葡萄城官网](https://demo.grapecity.com.cn/spreadjs/SpreadJSTutorial/#/samples)





